const commonjs = require('rollup-plugin-commonjs');
const vue = require('rollup-plugin-vue');
const babel = require('rollup-plugin-babel');
const path = require('path');
const glob = require('glob');

const projectRoot = path.join(__dirname, '..', '..');

module.exports = glob.sync(
  'bundles/*.js',
  { cwd: __dirname, realpath: true }
).map(file => ({
  external: ['vue', 'jquery', 'clipboard', 'vue-select', 'popperjs'],
  input: file,
  output: {
    file: path.join(projectRoot, 'tmp/frontend/javascripts/bundles/', path.basename(file)),
    format: 'iife',
    globals: {
      vue: 'Vue',
      jquery: 'window.$',
      clipboard: 'window.Clipboard',
      'vue-select': 'window.VueSelect',
      'popperjs': 'Popper',
    }
  },
  plugins: [
    commonjs(),
    vue(),
    babel({
      "exclude": "node_modules/**",
    }),
  ]
}));
